﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;
using PhoneApp1.Model;
using PhoneApp1.Resources;

namespace PhoneApp1
{
    public partial class MainPage : PhoneApplicationPage
    {
        private string idShot;
        private WebClient client;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            pbProgress.IsIndeterminate = true;
            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            client = new WebClient();
            client.DownloadStringCompleted += new DownloadStringCompletedEventHandler(shots_DownloadStringCompleted);
            client.DownloadStringAsync(new Uri("http://api.dribbble.com/shots/popular?page=1"));
        }

        private void shots_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var result = JsonConvert.DeserializeObject<Rootobject>(e.Result);
            llsShots.ItemsSource = result.shots;
            pbProgress.IsIndeterminate = false;
        }

        private void imgShot_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            this.idShot = (sender as Image).Tag.ToString();

            NavigationService.Navigate(new Uri("/ShotDetail.xaml?id=" + this.idShot, UriKind.Relative));
        }

        

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}