﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDesafioWP.Model
{
    public class Detail
    {
        
        public class Rootobject
        {
            public int id { get; set; }
            public string title { get; set; }
            public string description { get; set; }
            public int height { get; set; }
            public int width { get; set; }
            public int likes_count { get; set; }
            public int comments_count { get; set; }
            public int rebounds_count { get; set; }
            public string url { get; set; }
            public string short_url { get; set; }
            public int views_count { get; set; }
            public object rebound_source_id { get; set; }
            public string image_url { get; set; }
            public string image_teaser_url { get; set; }
            public string image_400_url { get; set; }
            public Player player { get; set; }
            public string created_at { get; set; }
        }

        public class Player
        {
            public int id { get; set; }
            public string name { get; set; }
            public string location { get; set; }
            public int followers_count { get; set; }
            public int draftees_count { get; set; }
            public int likes_count { get; set; }
            public int likes_received_count { get; set; }
            public int comments_count { get; set; }
            public int comments_received_count { get; set; }
            public int rebounds_count { get; set; }
            public int rebounds_received_count { get; set; }
            public string url { get; set; }
            public string avatar_url { get; set; }
            public string username { get; set; }
            public string twitter_screen_name { get; set; }
            public string website_url { get; set; }
            public int drafted_by_player_id { get; set; }
            public int shots_count { get; set; }
            public int following_count { get; set; }
            public string created_at { get; set; }
        }
    }
}
