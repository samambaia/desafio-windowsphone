﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;
using PhoneApp1.Model;

namespace PhoneApp1
{
    public partial class Page1 : PhoneApplicationPage
    {
        string idShot = string.Empty;

        public Page1()
        {
            InitializeComponent();
            pbProgress.IsIndeterminate = true;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            NavigationContext.QueryString.TryGetValue("id", out idShot);

            WebClient client = new WebClient();
            client.DownloadStringCompleted += new DownloadStringCompletedEventHandler(detail_DownloadStringCompleted);
            client.DownloadStringAsync(new Uri("http://api.dribbble.com/shots/" + this.idShot));
        }

        void detail_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            var result = JsonConvert.DeserializeObject<Shot>(e.Result);

            this.Dispatcher.BeginInvoke(() =>
                {
                    this.DataContext = result;
                    if (result.description != null)
                        webDescription.NavigateToString(result.description);

                    this.pbProgress.IsIndeterminate = false;
                }
                );
        }
    }
}