﻿#pragma checksum "C:\Users\Samambaia\Documents\Visual Studio 2013\Projects\PhoneApp1\PhoneApp1\ShotDetail.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "CC769E483C8324733E4ED136B114B639"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace PhoneApp1 {
    
    
    public partial class Page1 : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.ProgressBar pbProgress;
        
        internal System.Windows.Controls.Image imgShot;
        
        internal System.Windows.Controls.TextBlock txbTitle;
        
        internal System.Windows.Controls.TextBlock txbViewsCount;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal System.Windows.Shapes.Ellipse Avatar;
        
        internal System.Windows.Media.ImageBrush imgAvatar;
        
        internal System.Windows.Controls.TextBlock txbName;
        
        internal Microsoft.Phone.Controls.WebBrowser webDescription;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/PhoneApp1;component/ShotDetail.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.pbProgress = ((System.Windows.Controls.ProgressBar)(this.FindName("pbProgress")));
            this.imgShot = ((System.Windows.Controls.Image)(this.FindName("imgShot")));
            this.txbTitle = ((System.Windows.Controls.TextBlock)(this.FindName("txbTitle")));
            this.txbViewsCount = ((System.Windows.Controls.TextBlock)(this.FindName("txbViewsCount")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.Avatar = ((System.Windows.Shapes.Ellipse)(this.FindName("Avatar")));
            this.imgAvatar = ((System.Windows.Media.ImageBrush)(this.FindName("imgAvatar")));
            this.txbName = ((System.Windows.Controls.TextBlock)(this.FindName("txbName")));
            this.webDescription = ((Microsoft.Phone.Controls.WebBrowser)(this.FindName("webDescription")));
        }
    }
}

